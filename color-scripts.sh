#!/bin/sh

SCRIPT_NAME="Color Scripts"
SCRIPT_VERSION="0.1"

SCRIPT_HEADER="\n\e[1m${SCRIPT_NAME}\e[0m ${SCRIPT_VERSION}, a Shell Color Scripts Displayer"
SCRIPT_USAGE="\e[1;91mUsage:\e[0m ${0##*/} \e[1;93m[Options]\e[0m"
SCRIPT_OPTIONS="\e[1;93mOptions:\e[0m\n -h, --help\t\tShow this help message\n -V, --version\t\tDisplay script version\n -d, --directory\tSpecify a directory with color scripts"
HELP_MESSAGE="${SCRIPT_HEADER}\n${SCRIPT_USAGE}\n\n${SCRIPT_OPTIONS}\n"
VERSION_MESSAGE="%s version %s\n"

OPTION_NOT_RECOGNIZED_MESSAGE="\e[1;91m%s\e[0m is \e[1mnot\e[0m a valid option, try \e[1;93m--help\e[0m for help\n"

SCRIPTS_PATH="/home/santiago/Documents/color-scripts"

function display_color_script() {
    file_name=$(ls $SCRIPTS_PATH | shuf -n 1)
    file_path="${SCRIPTS_PATH}/${file_name}"
    file_extension=${file_name##*.}

    if [[ $file_extension == "sh" ]]; then
        $file_path
    else
        cat $file_path
    fi
}

while [[ "$1" =~ ^- ]]; do
	case "$1" in

		-h | --help) printf "$HELP_MESSAGE" & exit ;;

		-V | --version) printf "$VERSION_MESSAGE" "$SCRIPT_NAME" "$SCRIPT_VERSION" & exit ;;

		-d | --directory) SCRIPTS_PATH=$2 ;;

		-*) printf "$OPTION_NOT_RECOGNIZED_MESSAGE" "$1" & exit ;;

	esac

	shift
done

display_color_script